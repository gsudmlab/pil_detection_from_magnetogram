import os
import shutil
import multiprocessing
import numpy as np
from datetime import datetime
import logging
# global not_proceed_HARP
from Config import config
from Util import PilLogger
from PILExtractor.PilExtractor import PilExtractor
import pandas as pd


if __name__ == '__main__':
    
    """
    #1. Initial Logging file
    #2. Initial Input and Output folder path
    #3. Initial Not proceed HARP file
    #4. Obtain HARP number from Input Folder
    #5. Apply multiprocessing, select number processor
    #6. Run PIL detection
    #7. Return HARP number which not proceed in the detection and save to .csv file.
    """
    start=datetime.now()
    
    PilLogger.PilLogger()
    input_dir = config.Configuration().input_dir
    export_dir = config.Configuration().export_dir

#     def refresh_exportdir(export_dir):
#         for file in os.listdir(export_dir):
#             path = os.path.join(export_dir, file)
#             if os.path.isfile(path):
#                 os.remove(path)
#             else:
#                 shutil.rmtree(path)

#     refresh_exportdir(export_dir)

    not_proceed_harp_file = os.path.join(export_dir, "not_proceed_HARP.csv")
    
    time_df_path = export_dir+'harp_process_time.csv'
    
    harp_num = [name for name in os.listdir(input_dir) if os.path.isdir(os.path.join(input_dir, name))]
    
#     harp_num = [str(h) for h in harp_num if int(h) in range(7001,8001)]#for test use
    
#     harp_num = ["10001"]

#     harp_num = ['194']

#     Clean to remove .ipynb_checkpoints
    harp_num = [name for name in harp_num if not name.startswith('.')]

    #Select number of processor
    n_pool = multiprocessing.Pool(processes=12)
    
    #apply starmap()
#     _files = n_pool.starmap(PilExtractor().multi_process, [(f, export_dir) for f in harp_num])
    
#     n_pool.close()

    
    # call apply_async() without callback
    _files = [n_pool.apply_async(PilExtractor().multi_process, args=(f, export_dir)) for f in harp_num]

    # result_objects is a list of pool.ApplyResult objects
    _filesresults = [r.get() for r in _files]

    n_pool.close()
    n_pool.join()
#     not_proceed_HARP = np.array(not_proceed_HARP)

#     process_harp_time = []
#     _files = []

#     for h in harp_num:
        
#         st1 = datetime.now()
        
#         _file = PilExtractor().multi_process(h,export_dir)
        
#         _files.append(_file)
        
#         process_time_single = datetime.now()-st1
        
#         process_harp_time.append(process_time_single)
        
#         logging.info('Done HARP Processing Time {}'.format(process_time_single))
        
#         logging.info('Done HARP {}'.format(h))

    
#     time_df_dic = {'harp':harp_num, 'process time':process_harp_time}
#     time_df = pd.DataFrame(time_df_dic)
    
#     time_df.to_csv(time_df_path)
    
    not_proceed_HARP_num = [x for x in _filesresults if x is not None]    # return the HARP number which not proceed 
    
    np.savetxt(not_proceed_harp_file, not_proceed_HARP_num, delimiter=",",fmt='%i')
    print('Done')
    
    end  = datetime.now()-start
    logging.info("Processing time: {}".format(end))
