import numpy as np
from skimage.measure import moments_central, moments_normalized, moments_hu
import pandas as pd

class ConstructMetaData:
    def fractal_dimension(self, Z):
        # Only for 2d image
        assert (len(Z.shape) == 2)

        # From https://github.com/rougier/numpy-100 (#87)
        def boxcount(Z, k):
            S = np.add.reduceat(
                np.add.reduceat(Z, np.arange(0, Z.shape[0], k), axis=0),
                np.arange(0, Z.shape[1], k), axis=1)
            # We count non-empty (0) and non-full boxes (k*k)
            return len(np.where((S > 0) & (S < k * k))[0])

        # Transform Z into a binary array
        self.Z = Z.astype(bool)
        # Minimal dimension of image
        self.p = min(self.Z.shape)
        self.n = 2 ** np.floor(np.log(self.p) / np.log(2))
        # Extract the exponent
        self.n = int(np.log(self.n) / np.log(2))
        # Build successive box sizes (from 2**n+1 down to 2**1)
        self.sizes = 2 ** np.arange(self.n, 1, -1)
        # Actual box counting with decreasing size
        self.counts = []
        for self.size in self.sizes:
            self.counts.append(boxcount(self.Z, self.size))
        # Fit the successive log(sizes) with log (counts)
        self.coeffs = np.polyfit(np.log(self.sizes), np.log(self.counts), 1)
        return -self.coeffs[0]

    def hu_moments_sk(self, ls_pil):
        # if image is all false, huMoments equals NaN
        self.moments_pil = []
        for self.i, self.m in enumerate(ls_pil):
            if self.m is not None:
                self.mu = moments_central(np.float32(self.m))
                self.nu = moments_normalized(self.mu)
                self.huMoments = moments_hu(self.nu)
                self.moments_pil.append(self.huMoments)
            else:
                self.moments_pil.append(np.array(['nan', 'nan', 'nan', 'nan', 'nan', 'nan', 'nan']))
        self.df = pd.DataFrame(self.moments_pil, columns=['Hu1', 'Hu2', 'Hu3', 'Hu4', 'Hu5', 'Hu6', 'Hu7'])
        self.df.fillna('nan', inplace=True)
        return self.df

    def cov_matrix_eigenvalues(self, file_pil):
        if np.invert(file_pil.astype(bool)).all():
            return (np.nan, np.nan)
        else:
            self.y, self.x = np.nonzero(file_pil)  # x = columns index and y = row index
            self.x = self.x - np.mean(self.x)
            self.y = self.y - np.mean(self.y)
            self.coords = np.vstack([self.x, self.y])
            self.cov = np.cov(self.coords)
            self.evals, self.evecs = np.linalg.eig(self.cov)
            return (self.evals[0], self.evals[1])  # lambda1,lambda2

