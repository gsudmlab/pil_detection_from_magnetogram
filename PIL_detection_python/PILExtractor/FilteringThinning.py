import numpy as np
import pandas as pd
from skimage.morphology import thin
from skimage.measure import regionprops_table

class FilteringThinning:
    def single_strength(self, pil_df_n, submap_series_n):
        self.pil_strgt_lst = []
        for self.i, self.pil_row in pil_df_n.iterrows():
            self.r_idx = self.pil_row.coords[:, 0]
            self.col_idx = self.pil_row.coords[:, 1]
            self.pil_strength = sum(abs(submap_series_n.data[self.r_idx, self.col_idx]))  # single strength
            self.pil_strgt_lst.append(self.pil_strength)
        return self.pil_strgt_lst

    def filter_strength_pil(self, pil_df, s_t, pil_label):
        self.cut_label = pil_df[pil_df['cut_threshold'] > s_t].label.values  # threshold which contains 90% ~95% PIL flux
        self.cut_idx = np.isin(pil_label, self.cut_label)  # idividual pil which total strength not initial total flux
        pil_label[self.cut_idx] = 0  # set small PIL flux label to 0, only keep big PIL flux label
        return pil_label

    def filtering_by_strength(self, ls_pil_df, ls_pil_label, ls_orig_pil, ls_submap, threshold=0.95):
        self.ls_n_filtering_label = []
        for i, file in enumerate(ls_pil_df):
            if file is not None:
                file['strength'] = self.single_strength(file, ls_submap[i])
                file.sort_values(by=['strength'], ascending=False, inplace=True)
                file['cum_percent'] = file['strength'].cumsum() / sum(
                    abs(file['strength']))  # generate ['cum_percent'] column
                file['str_percent'] = file['strength'] / sum(abs(file['strength']))
                file['cut_threshold'] = file['cum_percent'] - file[
                    'str_percent']  # cut threshold (handle if exist minority PIL)
                file['strength_keep'] = file.apply(lambda row: row.cut_threshold <= threshold, axis=1)
                self.n_label = self.filter_strength_pil(file, threshold, ls_pil_label[i])  # return the label matrix satisfy the filtering threshold
                self.ls_n_filtering_label.append(self.n_label)
            else:
                self.ls_n_filtering_label.append(None)
        return self.ls_n_filtering_label  # return filtering(by strength) label matrix

    def thining_strength_label(self, ls_filter_label):
        self.ls_strength_binary_image = []
        self.ls_thin_dataframe = []
        self.ls_thin_binary_image = []
        for self.i, self.file in enumerate(ls_filter_label):
            if self.file is not None:
                self.ls_strength_binary_image.append(np.zeros(self.file.shape) + self.file)
                self.pil_thin_label = self.label_conn_thin(self.file)
                self.prop = ['label', 'centroid', 'orientation', 'major_axis_length', 'minor_axis_length', 'bbox', 'area',
                        'coords', 'image', 'bbox_area', 'perimeter', 'convex_area']
                self.props_table = regionprops_table(self.pil_thin_label, properties=self.prop)
                self.thin_binary = self.pil_thin_label
                self.ls_thin_dataframe.append(pd.DataFrame(self.props_table))
                self.ls_thin_binary_image.append(self.thin_binary)
            else:
                self.ls_strength_binary_image.append(None)
                self.ls_thin_dataframe.append(None)
                self.ls_thin_binary_image.append(None)
        return self.ls_strength_binary_image, self.ls_thin_dataframe, self.ls_thin_binary_image

    def label_conn_thin(self, filtered_orig_label):
        self.pil_thin = thin(filtered_orig_label)  # thin the original filttered label matrix, return the binary mask
        filtered_orig_label[~self.pil_thin] = 0  # keep the original label, and set non-thining part as 0
        return filtered_orig_label  # return the label mask after thinning after_thin_label(keep the same label id as before_thinning)

    def filtering_by_thinned_length(self, ls_thin_df, ls_thin_b_image, thin_threshold=14):
        for self.i, self.file in enumerate(ls_thin_df):
            if self.file is not None:
                self.cut_thin_label = self.file[self.file['area'] < thin_threshold].label.values  # filter pil by length threshold
                self.file['length_keep'] = self.file.apply(lambda row: row.area >= thin_threshold, axis=1)
                if len(self.cut_thin_label) > 0:
                    self.cut_idx = np.isin(ls_thin_b_image[self.i], self.cut_thin_label)
                    ls_thin_b_image[self.i][self.cut_idx] = 0  # remove thinned length after filtering
        return ls_thin_b_image  # return list of length filtering binary PIL image with label

    def df_merge(self, ls_str_df, ls_thin_df):
        self.ls_merge_df = []
        for self.i, self.file in enumerate(ls_str_df):
            if self.file is not None:
                self.thin_str_df = ls_thin_df[self.i].merge(self.file, left_on='label', right_on='label',
                                                  suffixes=('_thin', '_strength'))
                self.ls_merge_df.append(
                    self.thin_str_df[self.thin_str_df['length_keep'] == True])  # select length and strength == True
            else:
                self.ls_merge_df.append(None)
        return self.ls_merge_df


