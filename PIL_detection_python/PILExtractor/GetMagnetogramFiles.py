from datetime import datetime
import os
import sunpy.map
from Config.config import Configuration

class GetMagnetogramFiles:
    """
    Obtain Magnetogram file from inpur folder
    """
    def get_TS(self, file_name):
        try:
            self.parts = file_name.split('.')
            self.part = self.parts[3]
            self.year = self.part[0:4]
            self.month = self.part[4:6]
            self.day = self.part[6:8]
            self.hr = self.part[9:11]
            self.minute = self.part[11:13]
            self.sec = self.part[13:15]
            self.iso = self.year + '-' + self.month + '-' + self.day + 'T' + self.hr + ':' + self.minute + ':' + self.sec
            TS = datetime.fromisoformat(self.iso)
            return TS
        except:
            print("Something went wrong when writing to the file")
    
    
     #for mdi file timestamp
    def get_TS_mdi(self, file_name):
        
        try:
            
    
            self.time = file_name.split('.')[1].split('_')[3]
            TS = datetime.fromisoformat(self.time)
            return TS
        
        except:
            print("Wrong with timestamp")
    
    #for mdi files
    def get_mdi_files(self,mdi_num):
        
    
    #     distance = {}
    #print(threshold)
        self.TS = {}
        self.TS_list = []
        self.magnetogram_list = []
        os.chdir(Configuration().input_dir + str(mdi_num) + '/')
        self.files = os.listdir()
    
        self.magnetogram_files = sorted([self.file for self.file in self.files if 'aria_corrected_cea' in self.file])
        if self.magnetogram_files == None or self.magnetogram_files == []:
            return [], []
        self.first_flag = True
        self.dictionary_of_files = {}
    
    
    
        for self.f in self.magnetogram_files:
            
            self.TS[self.f] = self.get_TS_mdi(self.f)

            self.magnetogram_list.append(sunpy.map.Map(self.f))


        
        return list(self.TS.values()), self.magnetogram_list
    
    

    def get_magnetogram_files_twelve_min(self, harp_num):
        """
        input:
        harp_num: HARP number
        
        return
        list of Time stamp and list of HMI objects
        """
        
        self.TS = {}
        self.TS_list = []
        self.magnetogram_list = []
        os.chdir(Configuration().input_dir + str(harp_num) + '/')
        self.files = os.listdir()
        self.magnetogram_files = sorted([self.file for self.file in self.files if 'magnetogram' in self.file])
        if self.magnetogram_files == None or self.magnetogram_files == []:
            return [], []
        self.first_flag = True
        self.dictionary_of_files = {}
        for self.f in self.magnetogram_files:
            self.TS[self.f] = self.get_TS(self.f)
            self.magnetogram_list.append(sunpy.map.Map(self.f))
        return list(self.TS.values()), self.magnetogram_list
