from skimage.measure import label,regionprops_table
import pandas as pd
import numpy as np

class GenerateDfFromSeries:
    def PIL_dataframe(self, lst_pil):
        self.ls_pil_df = []
        self.ls_ob_labels = []
        for self.i, self.file in enumerate(lst_pil):
            if (np.all(self.file == False)):
                self.ls_pil_df.append(None)
                self.ls_ob_labels.append(None)
            else:
                self.ob_labels = label(self.file, connectivity=2)
                self.prop = ['label', 'centroid', 'orientation', 'major_axis_length', 'minor_axis_length', 'bbox', 'area',
                        'coords', 'image', 'bbox_area']
                self.props_table = regionprops_table(self.ob_labels, properties=self.prop)
                self.ls_pil_df.append(pd.DataFrame(self.props_table))
                self.ls_ob_labels.append(self.ob_labels)
        return self.ls_pil_df, self.ls_ob_labels  # return the list of original pil dataframe and pil label list
