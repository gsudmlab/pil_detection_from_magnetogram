import numpy as np
import astropy.units as u
from astropy.utils.exceptions import AstropyWarning
import warnings
from astropy.coordinates import SkyCoord
from sunpy.coordinates import frames

# Ignore too many FITSFixedWarnings
warnings.simplefilter('ignore', category=AstropyWarning)

class CornerCenterCheck:
    """
    Check weather the heliocentric angle distance with centroid, corner (bottom left and top right) of patch less than 70. 
    """
    
    def great_circle_distance(self, x, y):
        self.x = x * np.pi / 180
        self.y = y * np.pi / 180
        self.dlong = self.x[1] - self.y[1]
        self.den = np.sin(self.x[0]) * np.sin(self.y[0]) + np.cos(self.x[0]) * np.cos(self.y[0]) * np.cos(self.dlong)
        self.num = (np.cos(self.y[0]) * np.sin(self.dlong)) ** 2 + (
                np.cos(self.x[0]) * np.sin(self.y[0]) - np.sin(self.x[0]) * np.cos(self.y[0]) * np.cos(self.dlong)) ** 2
        self.sig = np.arctan2(np.sqrt(self.num), self.den) * 180 / np.pi
        return self.sig

    def heliocentric_angle(self, CRVAL1, CRVAL2, CRLN_OBS, CRLT_OBS):
        self.longitude = CRVAL1 - CRLN_OBS
        self.latitude = CRVAL2 - CRLT_OBS
        self.x = np.array([self.latitude, self.longitude])
        self.y = np.array([0., 0.])
        return self.great_circle_distance(self.x, self.y)

    def get_hc_angle(self, header):
        self.crval1 = header['CRVAL1']
        self.crval2 = header['CRVAL2']
        self.crln_obs = header['CRLN_OBS']
        self.crlt_obs = header['CRLT_OBS']
        self.hc_angle = self.heliocentric_angle(self.crval1, self.crval2, self.crln_obs, self.crlt_obs)
        header['HC_ANGLE'] = self.hc_angle
        return self.hc_angle

    def check_SPEI(self, TS_list, m_files):
        """
        input: list of Time stamp, list of HMI objects
        return: 
        qualified list of Time stamp and HMI objects with the heliocentric distantce of centroid <70.
        """
        if TS_list == [] or TS_list == None:
            return [], []
        self.list_of_files = []
        self.new_TS_list = []
        for self.time, self.value in zip(TS_list, m_files):
            self.angle = self.get_hc_angle(self.value.fits_header)
            if self.angle < 70:
                self.list_of_files += [self.value]
                self.new_TS_list += [self.time]
        return self.new_TS_list, self.list_of_files

    def patch_corner_check(self, TS_list, m_files):
        
        """
        input: list of Time stamp, list of HMI objects
        return: 
        qualified list of Time stamp and HMI objects with the heliocentric distantce of bottom left and top right corner <70.
        """
        self.ls_map = []
        self.ls_n_TS = []
        if TS_list == [] or TS_list == None:
            return [], []
        else:
            for self.t, self.file in zip(TS_list, m_files):
                self.ob_time = self.file.fits_header['DATE-OBS']
                self.bl_coor = self.coord_transformer_hsc(self.file.bottom_left_coord.lon.deg, self.file.bottom_left_coord.lat.deg, self.ob_time)
                self.tr_coor = self.coord_transformer_hsc(self.file.top_right_coord.lon.deg, self.file.top_right_coord.lat.deg, self.ob_time)
                self.a_lat_lon = np.array([self.bl_coor.lat.deg, self.bl_coor.lon.deg])  # bottom_left
                self.b_lat_lon = np.array([self.tr_coor.lat.deg, self.tr_coor.lon.deg])  # top_right
                self.a_circle_dist = self.great_circle_distance(self.a_lat_lon, np.array([0., 0.]))  # bottom_left
                self.b_circle_dist = self.great_circle_distance(self.b_lat_lon, np.array([0., 0.]))  # top_right
                if self.a_circle_dist < 70 and self.b_circle_dist < 70:  # bottom left and top_right corner
                    self.ls_map.append(self.file)
                    self.ls_n_TS.append(self.t)
            return self.ls_n_TS, self.ls_map

    def coord_transformer_hsc(self, x, y, obs_time):
        """
        coordinate transformation between HeliographicCarrington and HeliographicStonyhurst
        """
        
        self.x = x  # Carrington longitude
        self.y = y  # Carrington latitude
        self.c = SkyCoord(self.x * u.deg, self.y * u.deg, frame=frames.HeliographicCarrington, obstime=obs_time, observer="earth")  # )
        self.c_hgs = self.c.transform_to(frames.HeliographicStonyhurst)
        return self.c_hgs  # Tx,Ty
