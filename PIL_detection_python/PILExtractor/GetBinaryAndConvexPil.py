from skimage.morphology import convex_hull_image
from skimage.measure import label, regionprops_table
import pandas as pd
import numpy as np

class GetBinaryAndConvexPil:
    def ls_final_pil(self, ls_filter_thin_df, ls_s_filter_b):
        self.ls_final_binary_PIL = []
        self.ls_final_convex_PIL = []
        for self.i, self.file in enumerate(ls_filter_thin_df):
            if self.file is not None:
                self.f_label = self.filter_strength_length(self.file)
                self.f_lab_matrix = self.get_final_bib_pil(ls_s_filter_b[self.i], self.f_label)
                self.f_conv_image = self.get_convex_image(self.f_lab_matrix)
                self.ls_final_binary_PIL.append(self.f_lab_matrix)
                self.ls_final_convex_PIL.append(self.f_conv_image)
            else:
                self.ls_final_binary_PIL.append(None)
                self.ls_final_convex_PIL.append(None)
        return self.ls_final_binary_PIL, self.ls_final_convex_PIL

    def filter_strength_length(self, thin_df):
        # filter pil based on length keep label
        self.thin_label = set(list(thin_df[thin_df['length_keep'] == True].label.values))
        self.final_label = list(self.thin_label)
        return self.final_label

    def get_final_bib_pil(self, s_filter_b_image, final_label):
        self.f_lab_matrix = np.zeros(s_filter_b_image.shape) + s_filter_b_image
        self.remove_idx = np.isin(self.f_lab_matrix, final_label, invert=True)  #
        self.f_lab_matrix[self.remove_idx] = 0  # final binary image of PIL (after strength and thinning)
        return self.f_lab_matrix

    def get_convex_image(self, pil_binary):
        self.convex_b = convex_hull_image(pil_binary).astype('int')
        return self.convex_b

    def convex_pil_thin(self, ls_pil_thin_final):
        self.ls_convex_df = []
        self.ls_convex_thin_b = []
        for self.i, self.file in enumerate(ls_pil_thin_final):
            if self.file is not None:
                self.f_conv_image = self.get_convex_image(self.file)
                self.ob_labels = label(self.f_conv_image, connectivity=2)
                self.prop = ['label', 'centroid', 'orientation', 'major_axis_length', 'minor_axis_length', 'bbox', 'area',
                        'coords', 'image', 'bbox_area', 'perimeter']
                self.props_table = regionprops_table(self.ob_labels, properties=self.prop)
                self.ls_convex_df.append(pd.DataFrame(self.props_table))
                self.ls_convex_thin_b.append(self.f_conv_image)
            else:
                self.ls_convex_df.append(None)
                self.ls_convex_thin_b.append(None)
        return self.ls_convex_df, self.ls_convex_thin_b

    def convexity_pil(self, ls_convex_pil_df, ls_str_thin_df):
        self.ls_convexity_pil = []
        for self.i, self.file in enumerate(ls_convex_pil_df):
            if (self.file is not None) and (len(self.file) > 0):
                self.conv_val = self.file['perimeter'].values[0] / ls_str_thin_df[self.i]['area_thin'].sum()
                self.ls_convexity_pil.append(self.conv_val)
            else:
                self.ls_convexity_pil.append(str(np.nan))
        return self.ls_convexity_pil