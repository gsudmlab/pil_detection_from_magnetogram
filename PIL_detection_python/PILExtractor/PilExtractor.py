import numpy as np
import pandas as pd
import os
import logging
from PIL import Image
from PILExtractor.GeneratePilSeries import GeneratePilSeries
from PILExtractor.GenerateDfFromSeries import GenerateDfFromSeries
from PILExtractor.FilteringThinning import FilteringThinning
from PILExtractor.GetBinaryAndConvexPil import GetBinaryAndConvexPil
from PILExtractor.ConstructMetaData import ConstructMetaData
from PILExtractor.GetMagnetogramFiles import GetMagnetogramFiles
from PILExtractor.CornerCenterCheck import CornerCenterCheck
global not_proceed_HARP
from datetime import datetime

class PilExtractor:

    def detection_pil(self, TS_list, ls_files, i, gauss_f, len_f, TS_list_orig):
        self.time_format = "%Y-%m-%d-%H:%M:%S"
        self.TS_list, self.PIL_series_orig_n = GeneratePilSeries().PIL_series(TS_list, ls_files, pos_g_val=100, neg_g_val=-100)
        self.ls_pil_orig_n, self.ls_label_orig_n = GenerateDfFromSeries().PIL_dataframe(self.PIL_series_orig_n)
        self.ls_n_label =  FilteringThinning().filtering_by_strength(self.ls_pil_orig_n, self.ls_label_orig_n, self.PIL_series_orig_n, ls_files, threshold=0.95)
        self.ls_strength_binary_image, self.ls_thin_df, self.ls_thin_binary = FilteringThinning().thining_strength_label(self.ls_n_label)
        self.ls_thin_filter_b_image = FilteringThinning().filtering_by_thinned_length(self.ls_thin_df, self.ls_thin_binary, thin_threshold=14)
        self.ls_final_thin = [
            None if self.im is None else None if np.all(self.im == 0) else (self.im.astype(bool).astype(int) * 255).astype(np.uint8)
            for self.im in self.ls_thin_filter_b_image]
        self.dic_final_thin = {self.f_name.strftime(self.time_format): Image.fromarray(self.im) for (self.f_name, self.im) in
                          zip(self.TS_list, self.ls_final_thin)
                          if self.im is not None}
        self.ls_final_b_pil, self.ls_final_convex_pil = GetBinaryAndConvexPil().ls_final_pil(self.ls_thin_df, self.ls_strength_binary_image)
        self.ls_final_ropi = [
            None if self.im is None else None if np.all(self.im == 0) else (self.im.astype(bool).astype(int) * 255).astype(np.uint8)
            for self.im in self.ls_final_b_pil]
        self.dic_final_ropi = {self.f_name.strftime(self.time_format): Image.fromarray(self.im) for (self.f_name, self.im) in
                          zip(self.TS_list, self.ls_final_ropi)
                          if self.im is not None}
        # Merge Strength and Thinning df (after applying filter)
        self.ls_str_thin_df = FilteringThinning().df_merge(self.ls_pil_orig_n, self.ls_thin_df)
        self.number_pils = [len(self.df) if self.df is not None else 0 for self.df in self.ls_str_thin_df]
        print(len(self.number_pils))
        self.sum_pil_len = ['nan' if self.df is None else sum(self.df['area_thin']) if len(self.df) != 0 else 'nan' for self.df in
                       self.ls_str_thin_df]
        print(len(self.sum_pil_len))
        self.sum_ropi_area = ['nan' if self.df is None else sum(self.df['area_strength']) if len(self.df) != 0 else 'nan' for self.df in
                         self.ls_str_thin_df]
        print(len(self.sum_ropi_area))
        self.sum_mfs = ['nan' if self.df is None else round(sum(self.df['strength']), 2) if len(self.df) != 0 else 'nan' for self.df in
                   self.ls_str_thin_df]
        # convex hull df of PIL, and dataframe of convex hull PIL
        self.ls_convex_pil_df, self.ls_convex_pil_thin = GetBinaryAndConvexPil().convex_pil_thin(self.ls_thin_filter_b_image)  # Input: final_PIL(thinned)
        self.ls_final_conv_pil = [
            None if self.im is None else None if np.all(self.im == 0) else (self.im.astype(bool).astype(int) * 255).astype(np.uint8)
            for self.im in self.ls_convex_pil_thin]
        self.dic_final_conv_pil = {self.f_name.strftime(self.time_format): Image.fromarray(self.im) for (self.f_name, self.im) in
                              zip(self.TS_list, self.ls_final_conv_pil) if self.im is not None}
        self.ls_frac_dim_pil = [str(np.nan) if self.im is None else str(np.nan)
        if np.all(self.im == 0) else ConstructMetaData().fractal_dimension(self.im) for self.im in self.ls_thin_filter_b_image]
        self.hu_moments_df_sk = ConstructMetaData().hu_moments_sk(self.ls_thin_filter_b_image)
        self.ls_engin_val = [ConstructMetaData().cov_matrix_eigenvalues(self.im) if self.im is not None else (np.nan, np.nan) for self.im in
                        self.ls_thin_filter_b_image]
        # ratio of perimeter of convex hull of final PIL(thinned) and number of pixel convered of final PIL()
        self.ls_convexity_pil = GetBinaryAndConvexPil().convexity_pil(self.ls_convex_pil_df, self.ls_str_thin_df)
        self.pil_meta_data = {'time_stamp': self.TS_list,
                         'num_PILs': self.number_pils,
                         'sum_PIL_length': self.sum_pil_len, 'sum_RoPI_Area': self.sum_ropi_area,
                         'sum_mag_field_strength': self.sum_mfs, 'fra_dim': self.ls_frac_dim_pil,
                         'eige_vals': self.ls_engin_val, 'convexity': self.ls_convexity_pil}
        self.pil_meta_data_df = pd.DataFrame(self.pil_meta_data, columns=list(self.pil_meta_data.keys()))
        self.pil_n_df = pd.concat([self.pil_meta_data_df, self.hu_moments_df_sk], axis=1)
        self.pil_n_df['patch_shape'] = [self.im.data.shape for self.im in self.ls_files]
        self.pil_n_df.set_index('time_stamp', drop=True, inplace=True)
        self.new_idx = self.pil_n_df.index.union(TS_list_orig)
        self.pil_n_df = self.pil_n_df.reindex(self.new_idx, fill_value='N/A')
        self.pil_n_df['detection_flag'] = self.pil_n_df.apply(
            lambda x: 'no pil SPE' if x['num_PILs'] == 'N/A' else 'no pil E/W' if str(x['num_PILs'])
                                                                                  == '0' else 'detected', axis=1)
        return self.pil_n_df, self.dic_final_thin, self.dic_final_ropi, self.dic_final_conv_pil

    def multi_process(self, i, export_dir):
        print(str(i))
        st1 = datetime.now()
        logging.info("Start HARP {}".format(i))
        try:
            self.folder_name = os.mkdir(export_dir + '{}'.format(i))  # create folder for each HARP
        except:
            print(i, " is existed.")
            logging.info("Not proceed HARP due to {} is existed".format(i))
#             return int(i)
        else:
            
            try:
                print("start:{}".format(i))
#                 self.TS_list_orig, self.ls_files_orig = GetMagnetogramFiles().get_magnetogram_files_twelve_min(i)
                
                #for MDI files
                self.TS_list_orig, self.ls_files_orig = GetMagnetogramFiles().get_mdi_files(i)
                
                print("finish get files:{}".format(i))
                print(len(self.TS_list_orig))
                if len(self.TS_list_orig) > 0:
                    try:
                        self.TS_list, self.ls_files = CornerCenterCheck().check_SPEI(self.TS_list_orig, self.ls_files_orig)
                        print("center done")
                        self.TS_list, self.ls_files = CornerCenterCheck().patch_corner_check(self.TS_list, self.ls_files)
                        print("corner done")
                    except:
    #                     not_proceed_HARP.append(i)

                        print(
                            "center, corner check not finished")  # record folder number which didn't finish center and corner check
                        logging.error("Not Proceed AR due to Patch Validation not Finished: AR {}".format())
                        return int(i)

                    else:
                        if len(self.TS_list) != len(self.ls_files):
    #                         not_proceed_HARP.append(i)

                            logging.error("Not Proceed AR due to non-equal length file: {}".format(i))
                            return int(i)

                        elif len(self.TS_list) == 0:
                            self.export_folder_path = os.path.join(export_dir, str(i))
                            self.f_name = str(i) + '_pil'
                            self.not_proceed_df = pd.DataFrame(index=self.TS_list_orig, columns=[
                                "num_PILs,sum_PIL_length,sum_RoPI_Area,sum_mag_field_strength,fra_dim,eige_vals,convexity,Hu1,Hu2,Hu3,Hu4,Hu5,Hu6,Hu7,patch_shape,detection_flag"])
                            self.not_proceed_df['detection_flag'] = 'no pil SPE'
                            self.not_proceed_df.fillna('N/A')
                            self.not_proceed_df.to_csv('{0}.csv'.format(os.path.join(self.export_folder_path, self.f_name), index=True))
                            logging.info("None PIL due to no qualified AR patch, and saved to csv file: {}".format(i))
                        else:
                            try:
                                self.export_folder_path = os.path.join(export_dir, str(i))
                                self.pil_meta, self.pil_thin, self.ropi_map, self.pil_convex = self.detection_pil(self.TS_list, self.ls_files, i, 100, 14,
                                                                                         self.TS_list_orig)
                                
                                
                                if len(self.pil_thin) == len(self.ropi_map) == len(self.pil_convex):
                                
                                    self.f_name = str(i) + '_pil'
                                    self.pil_meta.to_csv('{0}.csv'.format(os.path.join(self.export_folder_path, self.f_name), index=True))



                                    if len(self.pil_thin) == len(self.ropi_map) == len(self.pil_convex) != 0:
                                        self.harp_num = i
                                        self.BLOS = ['PIL', 'RoPI', 'CHPIL']
                                        self.FST = '100G'
                                        self.LT = '14pix'
                                        for self.key in self.pil_thin:
                                            self.im_name = [
                                                'AR{0}_{1}_BLOS_{2}_FST{3}_LT{4}.png'.format(self.harp_num, self.key, blos, self.FST, self.LT) for blos in self.BLOS]
                                            self.im_path = [os.path.join(self.export_folder_path, self.n) for self.n in self.im_name]
                                            self.pil_thin[self.key].save(self.im_path[0], bits=1, optimize=True)  # pil image
                                            self.ropi_map[self.key].save(self.im_path[1], bits=1, optimize=True)  # RoPI image
                                            self.pil_convex[self.key].save(self.im_path[2], bits=1, optimize=True)  # Convex Hull PIL image


                                    ed1 = datetime.now() - st1
                                    print('Save Successfully for AR: ', i)
                                    logging.info("Save Successfully for AR: {}".format(i))
                                    logging.info("Processing Time for AR {}, {}".format(i,ed1))
                                 
                                
                                else:
                                    
                                    logging.error("Not Proceed AR {} due to: Not equal length image".format(i))
                                    return int(i)
                                
                                 
                            except:
                                print("Not finished")
    #                             not_proceed_HARP.append(i)
                                logging.error("Not Proceed AR {} due to: detection procedure interruppted".format(i))
                                return int(i)
                else:
    #                 not_proceed_HARP.append(i)
    #                 print(not_proceed_HARP)
                    print('Nothing!', i)
                    logging.error("Not Proceed AR  {} due to: None magnetogram files in AR folder".format(i))
                    return int(i)
            
            except:
                
                logging.debug("Not proceed AR {} due to : GetMagnetogramFiles not working".format(i))
                return int(i)
                
