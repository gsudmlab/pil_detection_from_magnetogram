import numpy as np
from skimage import feature
from skimage.morphology import square, dilation, thin

class GeneratePilSeries:
    def identify_pos_neg_region(self,fits_map, pos_gauss=100, neg_gauss=-100):
        self.pos_map = np.zeros(fits_map.data.shape)
        self.neg_map = np.zeros(fits_map.data.shape)
        np.warnings.filterwarnings('ignore')
        self.result_pos = np.where(fits_map.data >= pos_gauss)
        self.result_neg = np.where(fits_map.data <= neg_gauss)
        self.pos_map[self.result_pos[0], self.result_pos[1]] = 1
        self.neg_map[self.result_neg[0], self.result_neg[1]] = 1
        return self.pos_map, self.neg_map

    def edge_detection(self, binary_map):
        self.sig = 1
        self.edges = feature.canny(binary_map, sigma=self.sig)
        return self.edges

    def buff_edge(self, edges, size=4):
        self.selem = square(size)
        self.dilated_edges = dilation(edges, self.selem)
        return self.dilated_edges

    def pil_extraction(self, buff_pos, buff_neg, fits_map, thinning=True):
        self.pil_mask = np.invert(np.isnan(fits_map.data))
        self.pil_result = np.where(buff_pos & buff_neg & self.pil_mask)  # index(pixel) coordinates of PIL intersection
        self.pil_map = np.zeros(fits_map.data.shape)
        self.pil_map[self.pil_result[0], self.pil_result[1]] = 1
        if thinning == True:
            self.thinned_pil = thin(self.pil_map)
            return self.thinned_pil
        else:
            return self.pil_map

    def PIL_series(self, TS_list, ls_map, pos_g_val=100, neg_g_val=-100):
        self.new_TS_list = []
        self.pil_series_not_thin = []
        self.success_count = 0
        for self.i, self.sub_map in enumerate(ls_map):
            self.pos_map, self.neg_map = self.identify_pos_neg_region(self.sub_map, pos_gauss=pos_g_val, neg_gauss=neg_g_val)
            self.pos_edge = self.edge_detection(self.pos_map)
            self.neg_edge = self.edge_detection(self.neg_map)
            self.pos_dil_edge = self.buff_edge(self.pos_edge, size=4)
            self.neg_dil_edge = self.buff_edge(self.neg_edge, size=4)
            self.pil_final_not_thin = self.pil_extraction(self.pos_dil_edge, self.neg_dil_edge, self.sub_map, thinning=False)
            self.pil_series_not_thin.append(self.pil_final_not_thin)
            self.new_TS_list += [TS_list[self.i]]
            self.success_count += 1
        print("Success: ", self.success_count)
        print("Fail: ", len(ls_map) - self.success_count)
        return self.new_TS_list, self.pil_series_not_thin  # return the PIL series (not thinning) and submap of single HARP number
