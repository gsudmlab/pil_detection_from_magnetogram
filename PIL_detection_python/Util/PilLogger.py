import logging
import warnings
from Config.config import Configuration

class PilLogger:
    """
    Logging file setting
    """
    warnings.filterwarnings;('ignore')
    log_format = '%(asctime)s %(levelname)s: %(message)s'
    logging.basicConfig(filename=Configuration().log_location,
                            format=log_format, level=logging.DEBUG,
                            filemode='w')

