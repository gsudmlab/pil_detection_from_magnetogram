import time
import os


class Configuration:
    
    """
    Folder Configuration Setting

    log_dir_name: Folder path of logging file

    input_dir: Input folder path of AR Magnetograms

    export_dir: Output folder path of detected PIL

    """
    
    
    log_dir_name=r'/home/xmcai8/MDI_folder/'
    timestr = time.strftime("%Y%m%d-%H%M%S")
    filename = 'Log'+timestr
    format = 'log'
    log_location=os.path.join(log_dir_name, filename + "." + format)
    input_dir=r'/data/SOHO-MDI/MDI-CEA-Clip/'
    export_dir=r'/home/xmcai8/MDI_folder/'