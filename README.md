# Polarity Inversion Line (PIL) Detection and Feature Extraction

### Requirements:
* Python >= 3.6
* Environment setting: [environment_py.yml](environment_py.yml )
----
## Data Source:

----
### Configuration:
[[Config](PIL_detection_python/Config/config.py)]
Set up folder path of input,output,logging file

----
## Procedure:
[[PILExtractor.PilExtractor.py](PIL_detection_python/PILExtractor/PilExtractor.py)]


### Patch Validation
[[PILExtractor.CornerCenterCheck.py](PIL_detection_python/PILExtractor/CornerCenterCheck.py)]

```python
def check_SPEI(TS_list, ls_files)

def patch_corner_check(TS_list, ls_files)
```

### PIL Detection
[[PILExtractor.GeneratePilSeries.py](PIL_detection_python/PILExtractor/GeneratePilSeries.py),[PILExtractor.FilteringThinning.py](PIL_detection_python/PILExtractor/FilteringThinning.py)]

Step.1 Initial Magnetitude Field Strength
```python
def identify_pos_neg_region(self,fits_map, pos_gauss=100, neg_gauss=-100)
```

Step.2 Canny Edge Detection
```python
def edge_detection(self, binary_map)
```

Step.3 Morphological Dilation Operation
```python
def buff_edge(self, edges, size=4)
```

Step.4 Magnetitude Field Strength Filter
```python
def filtering_by_strength(self, ls_pil_df, ls_pil_label, ls_orig_pil, ls_submap, threshold=0.95)
```

Step.5 Morphological Thinning Operation
```python
def label_conn_thin(self, filtered_orig_label)
```

Step.6 PIL Length Filter
```python
def filtering_by_thinned_length(self, ls_thin_df, ls_thin_b_image, thin_threshold=14)
```


### Feature Extraction:
[[PILExtractor.ConstructMetaData.py](PIL_detection_python/PILExtractor/ConstructMetaData.py),[PILExtractor.GetBinaryAndConvexPil.py](PIL_detection_python/PILExtractor/GetBinaryAndConvexPil.py)]

Metadat includes:

num_PILs, sum_PIL_length, sum_RoPI_Area,sum_mag_field_strength, fra_dim, eige_vals, convexity,Hu1,Hu2,Hu3,Hu4,Hu5,Hu6,Hu7,patch_shape,detection_flag


	"num_PILs" -Number of PILs
	"sum_PIL_length" -Total size of PIL in the patch
	"sum_RoPI_Area" -Total number of pixel of region of Polarity Inversion
	"sum_mag_field_strength" -Total unsigned flux of covered by RoPI
	
	Shape descriptor:
	"fra_dim" -Fractal Dimension
	"eige_vals" -Eigenvalues
	"convexity" -Convexity
	"Hu1","Hu2","Hu3","Hu4","Hu5","Hu6","Hu7" -Hu moments
	
We use "detection_flag" to identify the detection status of PIL:
	
	"detected" - Indicate that PIL were detected by the detection procedure
	"no pil E/W" - Indicate that no PIL found due to weak magnetic field strength 
	"no pil SPE" - Indicate that no PIL detected due to the severe projection effect of HARP patch 

----
## Results and Outputs:
[PIL_sample_output](Sample_Output)
file format:


* #### Binary Mask (.png)

	* #### Region of PI (Region of Polarity Inversion)
	
	* #### Convex Hull of PIL
	
	* #### PIL Mask
	
* #### Meta Data (.csv)

----

## How to run the python script?

[main.py](PIL_detection_python/main.py)

